angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('MapCtrl', function($scope, $ionicLoading, $http) {
	
	var markersArr = []; // Create the markers you want to add and collect them into a array.
	var isWebView = ionic.Platform.isWebView();

	$scope.mapCenter = function(lat, lng) {
		initialLocation = new google.maps.LatLng(lat, lng);
        $scope.map.setCenter(initialLocation);
		//find the marker on coords and trigger click on it
		var marker;
		for (var i=0; i<markersArr.length; i++) {
			if (markersArr[i].position.lat() == lat && markersArr[i].position.lng() == lng) {
				marker = markersArr[i];
				break;
			}
		}
		if (marker) {
			var markerClicked = marker;
			new google.maps.event.trigger( markerClicked, 'click' );
		}
	}
	
	$scope.mapCreated = function(map) {
		$scope.map = map;
		
		//listeners fot Google Map
		google.maps.event.addDomListener($scope.map,'zoom_changed', function(e) {
			$scope.mapMarkers = getBounds($scope.map, markersArr);
			$scope.$digest();
		});
		
		google.maps.event.addDomListener($scope.map,'bounds_changed', function(e) {
			$scope.mapMarkers = getBounds($scope.map, markersArr);
			$scope.$digest();
		});
	};
	
	function initialize() {
		$http.get('https://api.myjson.com/bins/2k3ya', {cache: true}). 
			then(function (response){
					var markers = response.data;
					for(var i = 0; i < markers.length; i++){
						if (markers[i].lng && markers[i].lat){
								var myLatlng = new google.maps.LatLng(markers[i].lat,markers[i].lng);
								var marker = new google.maps.Marker({
									position: myLatlng,
									//icon: {
									//	path: google.maps.SymbolPath.CIRCLE,
									//	scale: 0,
									//},
									map: $scope.map,
									title: markers[i].name,
									labelAnchor: new google.maps.Point(10, 10),
									labelClass: "label",
									zipCode: markers[i].postcode,
									randomMarker: $scope.getRandomClass(),
									randomImg: $scope.getRandomImg()
									});

									markersArr.push(marker);
									getCoords(markers[i], marker, $scope.map);
								}			
							}
				}, function (error) {
					console.log("COULD NOT ACCESS DATA!", error.status, error.statusText);
				}).then(function(resp){
					var mcOptions = {gridSize: 50, maxZoom: 15};
					var mc = new MarkerClusterer($scope.map, markersArr, mcOptions);
				}).then(function(resp){
					$scope.mapMarkers = []; //array of map elements for menu 
					$scope.mapMarkers = getBounds($scope.map, markersArr);
				});
		
				
				
	}
	
	$scope.getRandomClass = function(){
		var tab = ['home', 'star', 'person-stalker', 'chatbubble-working'];
		return tab[Math.floor(Math.random() * (tab.length))]; 
	}
	
	$scope.getRandomImg = function(){
		var imgArr = ['1.jpg','2.png','3.png','4.jpg','5.jpg','6.jpg','7.jpg','8.png','9.jpg','10.png','11.jpg', '12.png','13.jpg', '14.png','15.jpg'];
		return imgArr[Math.floor(Math.random() * (imgArr.length))]; 
	}
  
	function getCoords(data, marker, map) {
		
		var infowindow = new google.maps.InfoWindow({
			content: data.name +" "+ data.postcode
		});
		
		google.maps.event.addDomListener(marker, 'click', function(e) {
			infowindow.open(map,this);
		});
		
		if (isWebView) {
			google.maps.event.addDomListener(marker, 'mouseover', function(e) {
				infowindow.open(map,this);
			});
		}
	}
	  
	function getBounds(map, markers) {
		$scope.markersMap = [];
		for (var i=0; i<markers.length; i++){
			if( map.getBounds().contains(markers[i].getPosition()) ){
				// code for showing your object, associated with markers[i]
				$scope.markersMap.push(markers[i]);
			}
		}
		//console.log($scope.markersMap)
		return $scope.markersMap;
	}
  
  if (document.readyState === "complete") {
	initialize();
  } else {
	google.maps.event.addDomListener(window, 'load', initialize);
  }
  
});
