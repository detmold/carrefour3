Ionic
=======

A Carefour web based project on Ionic created on September 14, 2015.

Installation
=======
1. Install node https://nodejs.org/en/
2. Install ionic http://ionicframework.com/getting-started/
3. ionic serve in cmd to run app in the browser

Develop on native platforms
=======
Android
=======
1. Install Android SDK for Windows https://developer.android.com/sdk/index.html
2. Go to cmd working directory (this is directory with README.md)
3. ionic platform add ios
4. ionic build android
5. To 9 build and emulate on device you should connect your Android device to computer and in cmd ionic run android 

IOS
=======
1. To build and emulate on IOS you must have Apple account
2. We usee PhoneGap to build and develop on IOS you must generate key

Windows
=======
1. To build you must have Windows 8 and  Visual Studio installed https://www.visualstudio.com/ 

